import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Emitters } from '../emitters/emitters';
import { CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  message = "You are not logged in"
  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.http.get('http://localhost:8000/api/user', {withCredentials: true}).subscribe(
      (res: any) => {
        this.message = `Hallo ${res.fullname}`
        Emitters.authEmitter.emit(true);
      },
      err => {
        this.message = 'You must be logged in'
        Emitters.authEmitter.emit(false)
      }
    )
  }

  todo = [
    'Go to work',
    'Code',
    'Migrasi HDD',
    'Patching Vulnerability'
  ];

  progress = [
    'Create Dashboard',
    'Audit',
    'Evidence Document',
    'CA Report',
    'Scan Vulnerability'
  ];

  review= [
    'CRL Report',
    'Vulnerability',
  ]

  done = [
    'Check Email',
    'Create Report'
  ];

  drop(event: CdkDragDrop<string[]>){
    if(event.previousContainer === event.container){
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex, 
        event.currentIndex
      )
    }
  }
}
